FROM python:3.7-slim
COPY . /app
RUN apt update
RUN apt install -y libpq-dev python-dev gcc
RUN pip install -r /app/requirements.txt
WORKDIR /app/
EXPOSE 5000
CMD python microblog.py